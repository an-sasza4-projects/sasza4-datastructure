meta:
  id: ammobin
  title: Ammo Database
  file-extension: bin
  endian: be


seq:
  - id: contents
    type: ammo
    repeat: eos


types:
  ammo:
    seq:
      - id: id
        type: s2
      - id: projectile_count
        type: s1
      - id: damage
        type: f4
      - id: damage_type
        type: s1
      - id: damage_radius
        type: f4
      - id: pierce
        type: s1
      - id: damage_dot
        type: f4
      - id: damage_dot_duration
        type: f4
      - id: arc_range
        type: f4
      - id: homing_angle
        type: f4
      - id: stun_duration
        type: f4
      - id: acceleration
        type: f4
      - id: acceleration_duration
        type: f4
      - id: asset_display_1
        type: s2
      - id: asset_trail
        type: s2
      - id: trail_eject_instance
        type: f4
      - id: asset_hit_wall
        type: s2
      - id: asset_hit_enemy
        type: s2
      - id: asset_display_2
        type: s2
      - id: asset_display_3
        type: s2
      - id: display_1_length_min
        type: f4
      - id: display_1_length_max
        type: f4
      - id: display_1_spacing_min
        type: f4
      - id: display_1_spacing_max
        type: f4
      - id: display_2_length_min
        type: f4
      - id: display_2_length_max
        type: f4
      - id: display_2_spacing_min
        type: f4
      - id: display_2_spacing_max
        type: f4
      - id: display_3_length_min
        type: f4
      - id: display_3_length_max
        type: f4
      - id: display_3_spacing_min
        type: f4
      - id: display_3_spacing_max
        type: f4
      - id: entity_collision_type
        type: s2
