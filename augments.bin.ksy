meta:
  id: augmentsbin
  title: Augment Database
  file-extension: bin
  endian: be


seq:
  - id: contents
    type: augment
    repeat: eos


types:
  augment:
    seq:
      - id: id
        type: s2
      - id: slots_len
        type: s1
      - id: slots
        type: s1
        repeat: expr
        repeat-expr: slots_len
      - id: type
        type: s1
      - id: grade
        type: s1
      - id: cost
        type: s4
      - id: csv_message_id
        type: s2
      - id: modifier_scale
        type: f4
