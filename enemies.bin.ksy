meta:
  id: enemiesbin
  title: Enemy Database
  file-extension: bin
  endian: be


seq:
  - id: contents
    type: enemy
    repeat: eos


types:
  enemy:
    seq:
      - id: id
        type: s4
      - id: name
        type: utf8str
      - id: variations_len
        type: s2
      - id: entity_diameter
        type: f4
      - id: stun_threshold
        type: f4
      - id: allow_gib
        type: boolean
      - id: gib_count
        type: f4
      - id: entity_mass
        type: f4
      - id: melee_range
        type: f4
      - id: turn_speed
        type: f4
      - id: asset_blood_ground
        type: s4
      - id: asset_blood_death
        type: s4
      - id: asset_blood_hit
        type: s4
      - id: drop_class
        type: s2
      - id: melee_damage_type
        type: s2
      - id: ranged_damage_type
        type: s2
      - id: variations
        type: enemy_variation
        repeat: expr
        repeat-expr: variations_len
  enemy_variation:
    seq:
      - id: version
        type: s4
      - id: drop_rate
        type: f4
      - id: health
        type: f4
      - id: melee_fire_rate
        type: f4
      - id: melee_damage
        type: f4
      - id: movement_speed
        type: f4
      - id: ranged_fire_rate
        type: f4
      - id: ranged_damage
        type: f4
      - id: ranged_projectile_speed
        type: f4
      - id: ranged_range
        type: f4
      - id: chemical_resist
        type: f4
      - id: energy_resist
        type: f4
      - id: physical_resist
        type: f4
      - id: thermal_resist
        type: f4
      - id: exp_rate
        type: f4
      - id: asset_death
        type: s4
      - id: asset_attack_ranged
        type: s4
      - id: asset_moving
        type: s4
      - id: asset_hit_1
        type: s4
      - id: asset_hit_2
        type: s4
      - id: asset_hit_3
        type: s4
      - id: asset_attack_melee_1
        type: s4
      - id: asset_attack_melee_2
        type: s4
      - id: asset_death_burned
        type: s4
  boolean:
    seq:
      - id: raw
        type: s1
    instances:
      result:
        value: raw != 0
  utf8str:
    seq:
      - id: len
        type: u2
      - id: result
        type: str
        size: len
        encoding: UTF-8
