meta:
  id: equipmentbin
  title: Equipment Database
  file-extension: bin
  endian: be


seq:
  - id: contents
    type: equipment
    repeat: eos


types:
  equipment:
    seq:
      - id: id
        type: s2
      - id: manufacturer_id
        type: s2
      - id: csv_name_id
        type: s2
      - id: slot_id
        type: s1
      - id: csv_description_id
        type: s2
      - id: rating
        type: s2
      - id: rarity
        type: s2
      - id: cost_base
        type: s4
      - id: physical_resist
        type: f4
      - id: thermal_resist
        type: f4
      - id: chemical_resist
        type: f4
      - id: movement_speed_scale
        type: f4
      - id: accuracy_scale
        type: f4
      - id: damage_scale
        type: f4
      - id: energy_scale
        type: f4
      - id: health_scale
        type: f4
      - id: health_regen
        type: f4
      - id: energy_regen
        type: f4
      - id: recovery_scale
        type: f4
      - id: reload_scale
        type: f4
      - id: melee_damage
        type: f4
      - id: melee_damage_type
        type: s1
      - id: asset_display_1
        type: s2
      - id: asset_display_2
        type: s2
      - id: asset_display_3
        type: s2
      - id: asset_display_4
        type: s2
      - id: asset_profile_1
        type: s2
      - id: asset_profile_2
        type: s2
      - id: asset_profile_3
        type: s2
      - id: asset_profile_4
        type: s2
      - id: asset_equipped_1
        type: s2
      - id: asset_equipped_2
        type: s2
      - id: asset_equipped_3
        type: s2
      - id: asset_equipped_4
        type: s2
      - id: grade_max
        type: s2
      - id: is_purchasable
        type: boolean
      - id: version
        type: s2
      - id: purchasable_level_min
        type: s2
      - id: grade_min
        type: s2
      - id: is_player_obtainable
        type: boolean
  boolean:
    seq:
      - id: raw
        type: s1
    instances:
      result:
        value: raw != 0
