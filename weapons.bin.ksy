meta:
  id: weaponsbin
  title: Weapon Database
  file-extension: bin
  endian: be


seq:
  - id: contents
    type: weapon
    repeat: eos


types:
  weapon:
    seq:
      - id: id
        type: s2
      - id: csv_name_id
        type: s2
      - id: csv_description_id
        type: s2
      - id: manufacturer_id
        type: s1
      - id: type_id
        type: s1
      - id: rating
        type: s2
      - id: grade_max
        type: s2
      - id: rarity
        type: s2
      - id: cost_base
        type: s4
      - id: range
        type: f4
      - id: accuracy
        type: f4
      - id: is_cone_shaped
        type: boolean
      - id: unk12
        type: f4
      - id: fire_rate
        type: f4
      - id: clip
        type: s2
      - id: reload_speed
        type: f4
      - id: movement_speed_scale
        type: f4
      - id: projectile_speed
        type: f4
      - id: trigger_type
        type: s1
      - id: ammo_id
        type: s2
      - id: asset_display
        type: s2
      - id: asset_profile
        type: s2
      - id: asset_eject
        type: s2
      - id: is_continuous_eject
        type: boolean
      - id: reload_type
        type: s2
      - id: pose_type
        type: s1
      - id: eject_offset_x
        type: f4
      - id: eject_offset_y
        type: f4
      - id: asset_eject_premium
        type: s2
      - id: eject_premium_offset_x
        type: f4
      - id: eject_premium_offset_y
        type: f4
      - id: gib_count
        type: f4
      - id: is_purchasable
        type: boolean
      - id: sound_id
        type: s2
      - id: asset_turret_destroyed
        type: s2
      - id: version
        type: s2
      - id: is_npc_weapon
        type: boolean
      - id: is_homing
        type: boolean
      - id: asset_anim_explosion
        type: s2
      - id: purchasable_level_min
        type: s2
      - id: asset_turret_body
        type: s2
      - id: asset_turret_head
        type: s2
      - id: asset_turret_deploy_body
        type: s2
      - id: asset_turret_deploy_head
        type: s2
      - id: is_turret
        type: boolean
      - id: turret_turn_speed
        type: f4
      - id: burst_speed
        type: f4
      - id: burst_count
        type: s2
      - id: turret_child_count
        type: s2
      - id: turret_child_id
        type: s2
      - id: sound_reload_id
        type: s4
      - id: consumable_type
        type: s2
      - id: consumable_order
        type: s2
      - id: grade_min
        type: s2
      - id: is_player_obtainable
        type: boolean
  boolean:
    seq:
      - id: raw
        type: s1
    instances:
      result:
        value: raw != 0
